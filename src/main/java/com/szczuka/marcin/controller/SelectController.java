package com.szczuka.marcin.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.szczuka.marcin.dao.RepositoryAccess;
import com.szczuka.marcin.domain.Query;
import com.szczuka.marcin.domain.QueryJoinParameter;
import com.szczuka.marcin.domain.QueryParameter;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcin on 12.04.16.
 */

@RestController
public class SelectController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    RepositoryAccess jdbcRepositoryAccess;

    @RequestMapping("api/select/list/{table_name}")
    public @ResponseBody
    ResponseEntity doSelectQueryForList(@PathVariable String table_name) throws JsonProcessingException {

        try {
            List<JSONObject> list = jdbcRepositoryAccess.simpleSelect(table_name);
            return ResponseEntity.ok(list);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "api/select", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity doSelectWithConstraints(@RequestBody Query query) {

        try {
            List<JSONObject> list = jdbcRepositoryAccess.queryWithConstraints(query);
            return ResponseEntity.ok(list);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "api/select/query", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity doSelectWithClientQuery(@RequestParam String query) {

        try {
            List<JSONObject> list = jdbcRepositoryAccess.queryFromClient(query);
            return ResponseEntity.ok(list);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    /* ONLY FOR TESTING */
    @RequestMapping("/select/test")
    public String getProperJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        Query newQuery = new Query();
        QueryParameter param = new QueryParameter();
        param.setName("name");
        param.setValue("Value");
        List<QueryParameter> listOfParam = new ArrayList<>();
        List<String> listOfColumns = new ArrayList<>();
        listOfParam.add(param);
        listOfColumns.add("col1");
        listOfColumns.add("col2");
        listOfColumns.add("col3");
        newQuery.setEqualToParameters(listOfParam);
        newQuery.setNotEqualToParameters(listOfParam);
        newQuery.setNotEqualToParameters(listOfParam);
        newQuery.setGreaterParameters(listOfParam);
        newQuery.setLowerParameters(listOfParam);
        newQuery.setLikeParameters(listOfParam);
        newQuery.setLimit(10);
        param.setName("name");
        //param.setValue(QueryOrderParameterEnum.ASCENDING);
        List<QueryParameter> newList = new ArrayList<>();
        newList.add(param);
        newQuery.setOrderParameters(newList);
        newQuery.setColumns(listOfColumns);
        QueryJoinParameter qjp = new QueryJoinParameter();
        qjp.setTable("table2");
        QueryParameter param1 = new QueryParameter();
        param1.setName("id");
        param1.setValue("table2.id");
        newList = new ArrayList<>();
        newList.add(param1);
        qjp.setJoinParameters(newList);
        newQuery.setJoinParameter(qjp);
        String json = mapper.writeValueAsString(newQuery);
        return json;
    }
}
