package com.szczuka.marcin.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by marcin on 13.04.16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Query {
    List<String> tables;
    QueryJoinParameter joinParameter;
    List<QueryParameter> equalToParameters;
    List<QueryParameter> equalToParametersOr;
    List<QueryParameter> notEqualToParameters;
    List<QueryParameter> notEqualToParametersOr;
    List<QueryParameter> greaterParameters;
    List<QueryParameter> greaterParametersOr;
    List<QueryParameter> lowerParameters;
    List<QueryParameter> lowerParametersOr;
    List<QueryParameter> likeParameters;
    List<QueryParameter> likeParametersOr;
    List<QueryParameter> orderParameters;
    Integer limit;
    List<String> columns;
    List<String> groupBy;
    Query parameterListsConcatWithOr;

    public List<String> getTables() {
        return tables;
    }

    public void setTables(List<String> tables) {
        this.tables = tables;
    }

    public QueryJoinParameter getJoinParameter() {
        return joinParameter;
    }

    public void setJoinParameter(QueryJoinParameter joinParameter) {
        this.joinParameter = joinParameter;
    }

    public List<QueryParameter> getEqualToParameters() {
        return equalToParameters;
    }

    public void setEqualToParameters(List<QueryParameter> equalToParameters) {
        this.equalToParameters = equalToParameters;
    }

    public List<QueryParameter> getNotEqualToParameters() {
        return notEqualToParameters;
    }

    public void setNotEqualToParameters(List<QueryParameter> notEqualToParameters) {
        this.notEqualToParameters = notEqualToParameters;
    }

    public List<QueryParameter> getGreaterParameters() {
        return greaterParameters;
    }

    public void setGreaterParameters(List<QueryParameter> greaterParameters) {
        this.greaterParameters = greaterParameters;
    }

    public List<QueryParameter> getLowerParameters() {
        return lowerParameters;
    }

    public void setLowerParameters(List<QueryParameter> lowerParameters) {
        this.lowerParameters = lowerParameters;
    }

    public List<QueryParameter> getLikeParameters() {
        return likeParameters;
    }

    public void setLikeParameters(List<QueryParameter> likeParameters) {
        this.likeParameters = likeParameters;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public List<String> getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(List<String> groupBy) {
        this.groupBy = groupBy;
    }

    public List<QueryParameter> getOrderParameters() {
        return orderParameters;
    }

    public void setOrderParameters(List<QueryParameter> orderParameters) {
        this.orderParameters = orderParameters;
    }

    public List<QueryParameter> getEqualToParametersOr() {
        return equalToParametersOr;
    }

    public void setEqualToParametersOr(List<QueryParameter> equalToParametersOr) {
        this.equalToParametersOr = equalToParametersOr;
    }

    public List<QueryParameter> getNotEqualToParametersOr() {
        return notEqualToParametersOr;
    }

    public void setNotEqualToParametersOr(List<QueryParameter> notEqualToParametersOr) {
        this.notEqualToParametersOr = notEqualToParametersOr;
    }

    public List<QueryParameter> getGreaterParametersOr() {
        return greaterParametersOr;
    }

    public void setGreaterParametersOr(List<QueryParameter> greaterParametersOr) {
        this.greaterParametersOr = greaterParametersOr;
    }

    public List<QueryParameter> getLowerParametersOr() {
        return lowerParametersOr;
    }

    public void setLowerParametersOr(List<QueryParameter> lowerParametersOr) {
        this.lowerParametersOr = lowerParametersOr;
    }

    public List<QueryParameter> getLikeParametersOr() {
        return likeParametersOr;
    }

    public void setLikeParametersOr(List<QueryParameter> likeParametersOr) {
        this.likeParametersOr = likeParametersOr;
    }

    public Query getParameterListsConcatWithOr() {
        return parameterListsConcatWithOr;
    }

    public void setParameterListsConcatWithOr(Query parameterListsConcatWithOr) {
        this.parameterListsConcatWithOr = parameterListsConcatWithOr;
    }

    @Override
    public String toString() {
        return "Query{" +
                "tables=" + tables +
                ", joinParameter=" + joinParameter +
                ", equalToParameters=" + equalToParameters +
                ", equalToParametersOr=" + equalToParametersOr +
                ", notEqualToParameters=" + notEqualToParameters +
                ", notEqualToParametersOr=" + notEqualToParametersOr +
                ", greaterParameters=" + greaterParameters +
                ", greaterParametersOr=" + greaterParametersOr +
                ", lowerParameters=" + lowerParameters +
                ", lowerParametersOr=" + lowerParametersOr +
                ", likeParameters=" + likeParameters +
                ", likeParametersOr=" + likeParametersOr +
                ", orderParameters=" + orderParameters +
                ", limit=" + limit +
                ", columns=" + columns +
                ", groupBy=" + groupBy +
                ", parameterListsConcatWithOr=" + parameterListsConcatWithOr +
                '}';
    }
}
