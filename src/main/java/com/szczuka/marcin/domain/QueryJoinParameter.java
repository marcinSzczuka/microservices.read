package com.szczuka.marcin.domain;

import java.util.List;

/**
 * Created by marcin on 14.04.16.
 */
public class QueryJoinParameter {
    private String table;
    private List<QueryParameter> joinParameters;

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public List<QueryParameter> getJoinParameters() {
        return joinParameters;
    }

    public void setJoinParameters(List<QueryParameter> joinParameters) {
        this.joinParameters = joinParameters;
    }
}
