package com.szczuka.marcin.domain;

/**
 * Created by marcin on 13.04.16.
 */
public enum QueryOrderParameterEnum {
    ASCENDING,DESCENDING;
}
