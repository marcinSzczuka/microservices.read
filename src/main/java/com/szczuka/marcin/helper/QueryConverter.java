package com.szczuka.marcin.helper;

import com.szczuka.marcin.domain.Query;
import com.szczuka.marcin.domain.QueryParameter;

import java.util.Iterator;
import java.util.List;

/**
 * Created by marcin on 13.04.16.
 */
public class QueryConverter {

    private static final String SELECT = "SELECT ";
    private static final String ASTERISK = " * ";
    private static final String FROM = "FROM ";
    private static final String WHERE = " WHERE ";
    private static final String JOIN = "JOIN $table_join ON";
    private static final String JOIN_NAME = "$join_name";
    private static final String JOIN_VALUE = "$join_value";
    private static final String AND = "AND ";
    private static final String OR = "OR ";
    private static final String LOWER = " < ";
    private static final String GREATER = " > ";
    private static final String LIMIT = "LIMIT ";
    private static final String ORDER = "ORDER BY ";
    private static final String DESC = "DESC ";
    private static final String ASC = "ASC ";
    private static final String SPACE = " ";
    private static final String EQUAL = " = ";
    private static final String NOT_EQUAL = " != ";
    private static final String LIKE = " LIKE '$pattern'";
    private static final String GROUP_BY = "GROUP BY ";
    private static final String COMMA = ", ";
    private static final String QUOTE = "'";

    private static final String ON_CONSTRAINT = "$table1.$column1 = $table2.$column2";

    private static final String COLUMN = "$column";
    private static final String TABLE= "$table";

    private static final String PARAM_EQ = "$param_eq_and_and";
    private static final String PARAM_EQ_AND_OR = "$param_eq_and_or";
    private static final String PARAM_EQ_OR_AND = "$param_eq_or_and";
    private static final String PARAM_EQ_OR = "$param_eq_or_or";

    private static final String VALUE_EQ = "$value_eq_and_and";
    private static final String VALUE_EQ_AND_OR = "$value_eq_and_or";
    private static final String VALUE_EQ_OR_AND = "$value_eq_or_and";
    private static final String VALUE_EQ_OR = "$value_eq_or_or";

    private static final String PARAM_NOTEQ = "$param_noteq_and_and";
    private static final String PARAM_NOTEQ_AND_OR = "$param_noteq_and_or";
    private static final String PARAM_NOTEQ_OR_AND = "$param_noteq_or_and";
    private static final String PARAM_NOTEQ_OR = "$param_noteq_or_or";

    private static final String VALUE_NOTEQ = "$value_noteq_and_and";
    private static final String VALUE_NOTEQ_AND_OR = "$value_noteq_and_or";
    private static final String VALUE_NOTEQ_OR_AND = "$value_noteq_or_and";
    private static final String VALUE_NOTEQ_OR = "$value_noteq_or_or";

    private static final String PARAM_GREATER = "$param_greater_and_and";
    private static final String PARAM_GREATER_AND_OR = "$param_greater_and_or";
    private static final String PARAM_GREATER_OR_AND = "$param_greater_or_and";
    private static final String PARAM_GREATER_OR = "$param_greater_or_or";

    private static final String VALUE_GREATER = "$value_greater_and_and";
    private static final String VALUE_GREATER_AND_OR = "$value_greater_and_or";
    private static final String VALUE_GREATER_OR_AND = "$value_greater_or_and";
    private static final String VALUE_GREATER_OR = "$value_greater_or_or";

    private static final String PARAM_LOWER = "$param_lower_and_and";
    private static final String PARAM_LOWER_AND_OR = "$param_lower_and_or";
    private static final String PARAM_LOWER_OR_AND = "$param_lower_or_and";
    private static final String PARAM_LOWER_OR = "$param_lower_or_or";

    private static final String VALUE_LOWER = "$value_lower_and_and";
    private static final String VALUE_LOWER_AND_OR = "$value_lower_and_or";
    private static final String VALUE_LOWER_OR_AND = "$value_lower_or_and";
    private static final String VALUE_LOWER_OR = "$value_lower_or_or";

    private static final String COLUMN_GROUP = "$column_group";

    private static final String PARAM_LIKE = "$param_like_and_and";
    private static final String PARAM_LIKE_AND_OR = "$param_like_and_or";
    private static final String PARAM_LIKE_OR_AND = "$param_like_or_and";
    private static final String PARAM_LIKE_OR = "$param_like_or_or";

    private static final String VALUE_LIKE = "$value_like_and_and";
    private static final String VALUE_LIKE_AND_OR = "$value_like_and_or";
    private static final String VALUE_LIKE_OR_AND = "$value_like_or_and";
    private static final String VALUE_LIKE_OR = "$value_like_or_or";

    private static final String COLUMN_ORDER = "$column_order";
    private static final String ORDER_TYPE = "$order_type";
    private static final String LIMIT_VALUE = "$limit_value";
    private static final String TABLE_JOIN = "$table_join";

    private static boolean isWhereInQuery = false;

    private static String generatePreparedStatement(Query query) {
        StringBuilder preparedStatement = new StringBuilder();
        preparedStatement.append(SELECT).append(SPACE);

        // SELECT
        if (query.getColumns() != null) {
            Iterator<String> iter = query.getColumns().iterator();
            int index = 0;
            while (iter.hasNext()) {
                iter.next();
                preparedStatement.append(COLUMN).append(index);
                if (iter.hasNext()) {
                    preparedStatement.append(COMMA);
                } else {
                    preparedStatement.append(SPACE);
                }
                index++;
            }
        } else {
            preparedStatement.append(ASTERISK);
        }

        // FROM
        preparedStatement.append(FROM).append(SPACE);
        if (query.getTables() != null && !query.getTables().isEmpty()) {
            Iterator<String> iter = query.getTables().iterator();
            int index = 0;
            while (iter.hasNext()) {
                iter.next();
                preparedStatement.append(TABLE).append(index);
                if (iter.hasNext()) {
                    preparedStatement.append(COMMA);
                } else {
                    preparedStatement.append(SPACE);
                }
                index++;
            }
        }

        // JOIN
        if (query.getJoinParameter() != null) {
            StringBuilder constraintQuery = new StringBuilder();
            List<QueryParameter> joinParameterList = query.getJoinParameter().getJoinParameters();
            Iterator<QueryParameter> iter = joinParameterList.iterator();
            int index = 0;

            preparedStatement.append(JOIN).append(SPACE);
            while (iter.hasNext()) {
                iter.next();
                //preparedStatement.append("$table1.").append("$column1").append(index).append(EQUAL).append("$table2").append(index).append(".").append("$column2").append(index);
                preparedStatement.append(JOIN_NAME).append(index).append(EQUAL).append(JOIN_VALUE + index);
                if (iter.hasNext()) {
                    constraintQuery.append(SPACE).append(AND);
                } else {
                    constraintQuery.append(SPACE);
                }
                index++;
            }
        }

        // WHERE
        isWhereInQuery = false;
        preparedStatement = whereAndConstraints(query, preparedStatement);
        preparedStatement = whereOrConstraints(query.getParameterListsConcatWithOr(), preparedStatement);

        // GROUP BY
        if (query.getGroupBy() != null && !query.getGroupBy().isEmpty()) {
            preparedStatement.append(SPACE).append(GROUP_BY);
            Iterator<String> iter = query.getGroupBy().iterator();
            int index = 0;
            while (iter.hasNext()) {
                iter.next();
                preparedStatement.append(COLUMN_GROUP).append(index);
                if (iter.hasNext()) {
                    preparedStatement.append(COMMA);
                } else {
                    preparedStatement.append(SPACE);
                }
                index++;
            }
        }

        // ORDER BY
        if (query.getOrderParameters() != null && !query.getOrderParameters().isEmpty()) {
            preparedStatement.append(SPACE).append(ORDER);
            Iterator<QueryParameter> iter = query.getOrderParameters().iterator();
            int index = 0;
            while (iter.hasNext()) {
                iter.next();
                preparedStatement.append(COLUMN_ORDER).append(index).append(SPACE).append(ORDER_TYPE).append(index);
                if (iter.hasNext()) {
                    preparedStatement.append(COMMA);
                } else {
                    preparedStatement.append(SPACE);
                }
                index++;
            }
        }

        // LIMIT
        if (query.getLimit() != null && query.getLimit() > 0) {
            preparedStatement.append(LIMIT).append(SPACE).append(LIMIT_VALUE);
        }

        return preparedStatement.toString();
    }

    public static String convert(Query query) {
        String preparedStatement = generatePreparedStatement(query);
        boolean needAnd = false;
        StringBuilder sb = new StringBuilder();
        sb.append(SELECT);

        // columns
        if (query.getColumns() != null && !query.getColumns().isEmpty()) {
            preparedStatement = putNames(preparedStatement, COLUMN, query.getColumns());
        }

        // table
        if (query.getTables() != null && !query.getTables().isEmpty()) {
            preparedStatement = putNames(preparedStatement, TABLE, query.getTables());
        }
        // join
        if (query.getJoinParameter() != null) {
            preparedStatement = preparedStatement.replace(TABLE_JOIN, query.getJoinParameter().getTable());
            List<QueryParameter> joinParameterList = query.getJoinParameter().getJoinParameters();
            Iterator<QueryParameter> iter = joinParameterList.iterator();
            int index = 0;
            while (iter.hasNext()) {
                QueryParameter parameter = iter.next();
                //preparedStatement = preparedStatement.replace("$table1", query.getTables().get(0));
                //preparedStatement = preparedStatement.replace("$column1"+index, parameter.getName());
                preparedStatement = preparedStatement.replace(JOIN_NAME+index, String.valueOf(parameter.getName()));
                //preparedStatement = preparedStatement.replace("$table2" + index, query.getJoinParameter().getTable());
                //preparedStatement = preparedStatement.replace("$column2" + index, String.valueOf(parameter.getValue()));
                preparedStatement = preparedStatement.replace(JOIN_VALUE+index, String.valueOf(parameter.getValue()));
                index++;
            }
        }

        // where
        preparedStatement = fillWhereWithAnd(query, preparedStatement);
        preparedStatement = fillWhereWithOr(query.getParameterListsConcatWithOr(), preparedStatement);

        // group by
        if (query.getGroupBy() != null && !query.getGroupBy().isEmpty()) {
            preparedStatement = putNames(preparedStatement, COLUMN_GROUP, query.getGroupBy());
        }

        // order by
        if (query.getOrderParameters() != null && !query.getOrderParameters().isEmpty()) {
            preparedStatement = putNamesAndValues(preparedStatement, COLUMN_ORDER, ORDER_TYPE, query.getOrderParameters());
        }

        // limit
        if (query.getLimit() != null && query.getLimit() > 0) {
           preparedStatement = preparedStatement.replace(LIMIT_VALUE, String.valueOf(query.getLimit()));
        }

        return preparedStatement;
    }

    private static String putNamesAndValues(String preparedStatement, String paramName, String paramValue, List<QueryParameter> parameterList) {
        Iterator<QueryParameter> iter = parameterList.iterator();
        int index = 0;
        while (iter.hasNext()) {
            QueryParameter parameter = iter.next();
            preparedStatement = preparedStatement.replace(paramName+index, parameter.getName());
            preparedStatement = preparedStatement.replace(paramValue+index, String.valueOf(parameter.getValue()));
            index++;
        }
        return preparedStatement;
    }

    private static String putNames(String preparedStatement, String paramName, List<String> parameterList) {
        Iterator<String> iter = parameterList.iterator();
        int index = 0;
        while (iter.hasNext()) {
            String parameter = iter.next();
            preparedStatement = preparedStatement.replace(paramName+index, parameter);
            index++;
        }
        return preparedStatement;
    }

    private static StringBuilder whereAndConstraints(Query query, StringBuilder preparedStatement) {
        NeedConcatStatus concat = new NeedConcatStatus();
        concat.setNeed(false);
        if ((query.getEqualToParameters() != null && !query.getEqualToParameters().isEmpty())
                || (query.getNotEqualToParameters() != null && !query.getNotEqualToParameters().isEmpty())
                || (query.getGreaterParameters() != null && !query.getGreaterParameters().isEmpty())
                || (query.getLowerParameters() != null && !query.getLowerParameters().isEmpty())
                || (query.getLikeParameters() != null && !query.getLikeParameters().isEmpty())
                || (query.getEqualToParametersOr() != null && !query.getEqualToParametersOr().isEmpty())
                || (query.getNotEqualToParametersOr() != null && !query.getNotEqualToParametersOr().isEmpty())
                || (query.getGreaterParametersOr() != null && !query.getGreaterParametersOr().isEmpty())
                || (query.getLowerParametersOr() != null && !query.getLowerParametersOr().isEmpty())
                || (query.getLikeParametersOr() != null && !query.getLikeParametersOr().isEmpty())) {

            preparedStatement.append(WHERE);
            isWhereInQuery = true;

            // and in lists
            // equal constraints
            preparedStatement = addWhereConstraint(query.getEqualToParameters(), EQUAL, PARAM_EQ, VALUE_EQ, AND, AND, preparedStatement, concat);
            // not equal
            preparedStatement = addWhereConstraint(query.getNotEqualToParameters(), NOT_EQUAL, PARAM_NOTEQ, VALUE_NOTEQ, AND, AND, preparedStatement, concat);
            // greater
            preparedStatement = addWhereConstraint(query.getGreaterParameters(), GREATER, PARAM_GREATER, VALUE_GREATER, AND, AND, preparedStatement, concat);
            // lower
            preparedStatement = addWhereConstraint(query.getLowerParameters(), LOWER, PARAM_LOWER, VALUE_LOWER, AND, AND, preparedStatement, concat);
            // like
            if (query.getLikeParameters() != null && !query.getLikeParameters().isEmpty()) {
                if (concat.isNeed()) {
                    preparedStatement.append(SPACE).append(AND);
                }
                concat.setNeed(true);
                Iterator<QueryParameter> iter = query.getLikeParameters().iterator();
                int index = 0;
                while (iter.hasNext()) {
                    iter.next();
                    preparedStatement.append(PARAM_LIKE).append(index).append(LIKE.replace("$pattern", VALUE_LIKE + index));
                    if (iter.hasNext()) {
                        preparedStatement.append(SPACE).append(AND);
                    } else {
                        preparedStatement.append(SPACE);
                    }
                    index++;
                }
            }

            // equal constraints
            preparedStatement = addWhereConstraint(query.getEqualToParametersOr(), EQUAL, PARAM_EQ_AND_OR, VALUE_EQ_AND_OR, AND, OR, preparedStatement, concat);
            // not equal
            preparedStatement = addWhereConstraint(query.getNotEqualToParametersOr(), NOT_EQUAL, PARAM_NOTEQ_AND_OR, VALUE_NOTEQ_AND_OR, AND, OR, preparedStatement, concat);
            // greater
            preparedStatement = addWhereConstraint(query.getGreaterParametersOr(), GREATER, PARAM_GREATER_AND_OR, VALUE_GREATER_AND_OR, AND, OR, preparedStatement, concat);
            // lower
            preparedStatement = addWhereConstraint(query.getLowerParametersOr(), LOWER, PARAM_LOWER_AND_OR, VALUE_LOWER_AND_OR, AND, OR, preparedStatement, concat);

            // or in lists
            // like
            if (query.getLikeParametersOr() != null && !query.getLikeParametersOr().isEmpty()) {
                if (concat.isNeed()) {
                    preparedStatement.append(SPACE).append(AND);
                }
                concat.setNeed(true);
                Iterator<QueryParameter> iter = query.getLikeParametersOr().iterator();
                int index = 0;
                while (iter.hasNext()) {
                    iter.next();
                    preparedStatement.append(PARAM_LIKE_AND_OR).append(index).append(LIKE.replace("$pattern", VALUE_LIKE_AND_OR + index));
                    if (iter.hasNext()) {
                        preparedStatement.append(SPACE).append(OR);
                    } else {
                        preparedStatement.append(SPACE);
                    }
                    index++;
                }
            }
        }
        return preparedStatement;
    }

    private static StringBuilder whereOrConstraints(Query query, StringBuilder preparedStatement) {
        //Boolean needOr = false;
        NeedConcatStatus concat = new NeedConcatStatus();
        concat.setNeed(false);
        if (query != null && ((query.getEqualToParameters() != null && !query.getEqualToParameters().isEmpty())
                || (query.getNotEqualToParameters() != null && !query.getNotEqualToParameters().isEmpty())
                || (query.getGreaterParameters() != null && !query.getGreaterParameters().isEmpty())
                || (query.getLowerParameters() != null && !query.getLowerParameters().isEmpty())
                || (query.getLikeParameters() != null && !query.getLikeParameters().isEmpty())
                || (query.getEqualToParametersOr() != null && !query.getEqualToParametersOr().isEmpty())
                || (query.getNotEqualToParametersOr() != null && !query.getNotEqualToParametersOr().isEmpty())
                || (query.getGreaterParametersOr() != null && !query.getGreaterParametersOr().isEmpty())
                || (query.getLowerParametersOr() != null && !query.getLowerParametersOr().isEmpty())
                || (query.getLikeParametersOr() != null && !query.getLikeParametersOr().isEmpty()))) {

            if(!isWhereInQuery) {
                preparedStatement.append(WHERE);
            } else {
                concat.setNeed(true);
            }
            // and in lists
            // equal constraints
            preparedStatement = addWhereConstraint(query.getEqualToParameters(), EQUAL, PARAM_EQ_OR_AND, VALUE_EQ_OR_AND, OR, AND, preparedStatement, concat);
            // not equal
            preparedStatement = addWhereConstraint(query.getNotEqualToParameters(), NOT_EQUAL, PARAM_NOTEQ_OR_AND, VALUE_NOTEQ_OR_AND, OR, AND, preparedStatement, concat);
            // greater
            preparedStatement = addWhereConstraint(query.getGreaterParameters(), GREATER, PARAM_GREATER_OR_AND, VALUE_GREATER_OR_AND, OR, AND, preparedStatement, concat);
            // lower
            preparedStatement = addWhereConstraint(query.getLowerParameters(), LOWER, PARAM_LOWER_OR_AND, VALUE_LOWER_OR_AND, OR, AND, preparedStatement, concat);
            // like
            if (query.getLikeParameters() != null && !query.getLikeParameters().isEmpty()) {
                if (concat.isNeed()) {
                    preparedStatement.append(SPACE).append(OR);
                }
                concat.setNeed(true);
                Iterator<QueryParameter> iter = query.getLikeParameters().iterator();
                int index = 0;
                while (iter.hasNext()) {
                    iter.next();
                    preparedStatement.append(PARAM_LIKE_OR_AND).append(index).append(LIKE.replace("$pattern", VALUE_LIKE_OR_AND + index));
                    if (iter.hasNext()) {
                        preparedStatement.append(SPACE).append(AND);
                    } else {
                        preparedStatement.append(SPACE);
                    }
                    index++;
                }
            }

            // or in lists
            // equal constraints
            preparedStatement = addWhereConstraint(query.getEqualToParametersOr(), EQUAL, PARAM_EQ_OR, VALUE_EQ_OR, OR, OR, preparedStatement, concat);
            // not equal
            preparedStatement = addWhereConstraint(query.getNotEqualToParametersOr(), NOT_EQUAL, PARAM_NOTEQ_OR, VALUE_NOTEQ_OR, OR, OR, preparedStatement, concat);
            // greater
            preparedStatement = addWhereConstraint(query.getGreaterParametersOr(), GREATER, PARAM_GREATER_OR, VALUE_GREATER_OR, OR, OR, preparedStatement, concat);
            // lower
            preparedStatement = addWhereConstraint(query.getLowerParametersOr(), LOWER, PARAM_LOWER_OR, VALUE_LOWER_OR, OR, OR, preparedStatement, concat);
            // like
            //preparedStatement = addWhereConstraint(query.getLikeParametersOr(), LIKE,PARAM_LIKE_OR, VALUE_LIKE_OR, OR, OR, preparedStatement, concat);
            if (query.getLikeParametersOr() != null && !query.getLikeParametersOr().isEmpty()) {
                if (concat.isNeed()) {
                    preparedStatement.append(SPACE).append(OR);
                }
                concat.setNeed(true);
                Iterator<QueryParameter> iter = query.getLikeParametersOr().iterator();
                int index = 0;
                while (iter.hasNext()) {
                    iter.next();
                    preparedStatement.append(PARAM_LIKE_OR).append(index).append(LIKE.replace("$pattern", VALUE_LIKE_OR + index));
                    if (iter.hasNext()) {
                        preparedStatement.append(SPACE).append(OR);
                    } else {
                        preparedStatement.append(SPACE);
                    }
                    index++;
                }
            }
        }
        return preparedStatement;
    }


    private static String fillWhereWithAnd(Query query, String preparedStatement) {
        if ((query.getEqualToParameters() != null && !query.getEqualToParameters().isEmpty())
                || (query.getNotEqualToParameters() != null && !query.getNotEqualToParameters().isEmpty())
                || (query.getGreaterParameters() != null && !query.getGreaterParameters().isEmpty())
                || (query.getLowerParameters() != null && !query.getLowerParameters().isEmpty())
                || (query.getLikeParameters() != null && !query.getLikeParameters().isEmpty())
                || (query.getEqualToParametersOr() != null && !query.getEqualToParametersOr().isEmpty())
                || (query.getNotEqualToParametersOr() != null && !query.getNotEqualToParametersOr().isEmpty())
                || (query.getGreaterParametersOr() != null && !query.getGreaterParametersOr().isEmpty())
                || (query.getLowerParametersOr() != null && !query.getLowerParametersOr().isEmpty())
                || (query.getLikeParametersOr() != null && !query.getLikeParametersOr().isEmpty())) {

            // equal constraints
            if (query.getEqualToParameters() != null && !query.getEqualToParameters().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_EQ, VALUE_EQ, query.getEqualToParameters());
            }
            // not equal
            if (query.getNotEqualToParameters() != null && !query.getNotEqualToParameters().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_NOTEQ, VALUE_NOTEQ, query.getNotEqualToParameters());
            }

            // greater
            if (query.getGreaterParameters() != null && !query.getGreaterParameters().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_GREATER, VALUE_GREATER, query.getGreaterParameters());
            }

            // lower
            if (query.getLowerParameters() != null && !query.getLowerParameters().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_LOWER, VALUE_LOWER, query.getLowerParameters());
            }

            // like
            if (query.getLikeParameters() != null && !query.getLikeParameters().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_LIKE, VALUE_LIKE, query.getLikeParameters());
            }

            // equal constraints
            if (query.getEqualToParametersOr() != null && !query.getEqualToParametersOr().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_EQ_AND_OR, VALUE_EQ_AND_OR, query.getEqualToParametersOr());
            }
            // not equal
            if (query.getNotEqualToParametersOr() != null && !query.getNotEqualToParametersOr().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_NOTEQ_AND_OR, VALUE_NOTEQ_AND_OR, query.getNotEqualToParametersOr());
            }

            // greater
            if (query.getGreaterParametersOr() != null && !query.getGreaterParametersOr().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_GREATER_AND_OR, VALUE_GREATER_AND_OR, query.getGreaterParametersOr());
            }

            // lower
            if (query.getLowerParametersOr() != null && !query.getLowerParametersOr().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_LOWER_AND_OR, VALUE_LOWER_AND_OR, query.getLowerParametersOr());
            }

            // like
            if (query.getLikeParametersOr() != null && !query.getLikeParametersOr().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_LIKE_AND_OR, VALUE_LIKE_AND_OR, query.getLikeParametersOr());
            }
        }
        return preparedStatement;
    }

    private static String fillWhereWithOr(Query query, String preparedStatement) {
        if (query != null && ((query.getEqualToParametersOr() != null && !query.getEqualToParametersOr().isEmpty())
                || (query.getNotEqualToParametersOr() != null && !query.getNotEqualToParametersOr().isEmpty())
                || (query.getGreaterParametersOr() != null && !query.getGreaterParametersOr().isEmpty())
                || (query.getLowerParametersOr() != null && !query.getLowerParametersOr().isEmpty())
                || (query.getLikeParametersOr() != null && !query.getLikeParametersOr().isEmpty())
                || (query.getEqualToParameters() != null && !query.getEqualToParameters().isEmpty())
                || (query.getNotEqualToParameters() != null && !query.getNotEqualToParameters().isEmpty())
                || (query.getGreaterParameters() != null && !query.getGreaterParameters().isEmpty())
                || (query.getLowerParameters() != null && !query.getLowerParameters().isEmpty())
                || (query.getLikeParameters() != null && !query.getLikeParameters().isEmpty()))) {

            // equal constraints
            if (query.getEqualToParameters() != null && !query.getEqualToParameters().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_EQ_OR_AND, VALUE_EQ_OR_AND, query.getEqualToParameters());
            }
            // not equal
            if (query.getNotEqualToParameters() != null && !query.getNotEqualToParameters().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_NOTEQ_OR_AND, VALUE_NOTEQ_OR_AND, query.getNotEqualToParameters());
            }

            // greater
            if (query.getGreaterParameters() != null && !query.getGreaterParameters().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_GREATER_OR_AND, VALUE_GREATER_OR_AND, query.getGreaterParameters());
            }

            // lower
            if (query.getLowerParameters() != null && !query.getLowerParameters().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_LOWER_OR_AND, VALUE_LOWER_OR_AND, query.getLowerParameters());
            }

            // like
            if (query.getLikeParameters() != null && !query.getLikeParameters().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_LIKE_OR_AND, VALUE_LIKE_OR_AND, query.getLikeParameters());
            }

            // equal constraints
            if (query.getEqualToParametersOr() != null && !query.getEqualToParametersOr().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_EQ_OR, VALUE_EQ_OR, query.getEqualToParametersOr());
            }
            // not equal
            if (query.getNotEqualToParametersOr() != null && !query.getNotEqualToParametersOr().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_NOTEQ_OR, VALUE_NOTEQ_OR, query.getNotEqualToParametersOr());
            }

            // greater
            if (query.getGreaterParametersOr() != null && !query.getGreaterParametersOr().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_GREATER_OR, VALUE_GREATER_OR, query.getGreaterParametersOr());
            }

            // lower
            if (query.getLowerParametersOr() != null && !query.getLowerParametersOr().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_LOWER_OR, VALUE_LOWER_OR, query.getLowerParametersOr());
            }

            // like
            if (query.getLikeParametersOr() != null && !query.getLikeParametersOr().isEmpty()) {
                preparedStatement = putNamesAndValues(preparedStatement, PARAM_LIKE_OR, VALUE_LIKE_OR, query.getLikeParametersOr());
            }
        }
        return preparedStatement;
    }

    private static StringBuilder addWhereConstraint(List<QueryParameter> parameterList,
                                                    String constraint,
                                                    String constraintParam,
                                                    String constraintValue,
                                                    String concatOuter,
                                                    String concatInner,
                                                    StringBuilder preparedStatement,
                                                    NeedConcatStatus concatStatus) {
        if (parameterList != null && !parameterList.isEmpty()) {

            if (concatStatus.isNeed()) {
                preparedStatement.append(SPACE).append(concatOuter);
            }
            concatStatus.setNeed(true);

            Iterator<QueryParameter> iter = parameterList.iterator();
            int index = 0;
            while (iter.hasNext()) {
                iter.next();
                preparedStatement.append(constraintParam).append(index).append(constraint).append(QUOTE).append(constraintValue).append(index).append(QUOTE);
                if (iter.hasNext()) {
                    preparedStatement.append(SPACE).append(concatInner);
                } else {
                    preparedStatement.append(SPACE);
                }
                index++;
            }
        }
        return preparedStatement;
    }
 }