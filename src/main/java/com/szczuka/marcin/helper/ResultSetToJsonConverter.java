package com.szczuka.marcin.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * Created by marcin on 12.04.16.
 */
public class ResultSetToJsonConverter {


    public static String convert(ResultSet rs) throws SQLException {
        ObjectNode node = new ObjectMapper().createObjectNode();
        ResultSetMetaData rsm = rs.getMetaData();

        if(rs.next()) {
            do {
                int columnCnt = rsm.getColumnCount();
                for(int i=1; i<=columnCnt; i++) {
                    String columnName = rsm.getColumnName(i);
                    node.put(columnName, rs.getString(columnName));
                }
            } while (rs.next());
        }

        return node.toString();
    }

}
