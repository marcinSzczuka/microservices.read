package com.szczuka.marcin.helper;

/**
 * Created by marcin on 13.06.16.
 */
public class NeedConcatStatus {
    boolean need;

    public boolean isNeed() {
        return need;
    }

    public void setNeed(boolean need) {
        this.need = need;
    }
}
