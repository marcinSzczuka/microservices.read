package com.szczuka.marcin.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.json.simple.JSONObject;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * Created by marcin on 12.04.16.
 */
public class JsonRowMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet rs, int i) throws SQLException {
        ResultSetMetaData rsm = rs.getMetaData();
        //ObjectNode node = new ObjectMapper().createObjectNode();
        JSONObject object = new JSONObject();
        int columnCnt = rsm.getColumnCount();
        for(int j=1; j<=columnCnt; j++) {
            String columnName = rsm.getColumnName(j);
            //node.put(columnName, rs.getString(columnName));
            object.put(columnName, rs.getString(columnName));

        }
        return object;
    }
}
