package com.szczuka.marcin.dao;

import com.szczuka.marcin.domain.Query;
import com.szczuka.marcin.helper.JsonRowMapper;
import com.szczuka.marcin.helper.QueryConverter;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by marcin on 13.04.16.
 */
@Repository
public class RepositoryAccess {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private String SIMPLE_SELECT_QUERY = "SELECT * FROM $tableName";

    public List<JSONObject> simpleSelect (String tableName) {
        String query = SIMPLE_SELECT_QUERY.replace("$tableName", tableName);
        List<JSONObject> objectNodeList = jdbcTemplate.query(query, new JsonRowMapper());
        return objectNodeList;
    }

    public List<JSONObject> queryWithConstraints(Query query) {
        String queryString = QueryConverter.convert(query);
        List<JSONObject> objectNodeList = jdbcTemplate.query(queryString, new JsonRowMapper());
        return objectNodeList;
    }

    public List<JSONObject> queryFromClient(String query) {
        List<JSONObject> objectNodeList = jdbcTemplate.query(query, new JsonRowMapper());
        return objectNodeList;
    }
}
